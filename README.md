# Redes de Comutadoras
## FCiencias, UNAM | Semestre 2022-1

Prácticas realizadas en el curso de Redes de Computadoras.

## Autores :busts_in_silhouette:
-  Daniel : [danielcastillo59814]( https://gitlab.com/danielcastillo59814 )

-  Ramon : [RamonCz]( https://gitlab.com/RamonCz )

-  Marco : [marcoantonio1999]( https://gitlab.com/marcoantonio1999 )

-  Karla : [KarlaDSJ]( https://gitlab.com/KarlaDSJ )

