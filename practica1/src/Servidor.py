import socket
import os

class Servidor:
    """
    Servidor, queda atento a peticiones 
    """
    def __init__(self) -> None:
        # Obtener la dirección IP del equipo (conexiones externas)
        # Para conexiones locales usar localhost, 127.0.0.1
        self.host = socket.gethostbyname(socket.gethostname())
        # Definir el puerto a la escucha
        self.port = 10000
        # Instanciar la clase socket
        self.mi_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #Agregando abndera para que la direccion pueda ser usada 
        #En la sigueintes ejecuciones de los scripts
        self.mi_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        # Enlazar el socket a la interfaz (configuración)
        self.mi_socket.bind((self.host, self.port))
        self.correo = ""

    def guardar_correo(self) -> None:
        """Guarda los correos que enviados por el cliente
         en la carpeta HOME del usuario Linux"""
        #Obtenemos la carpeta HOME del usuario Linux
        os.path
        user = str(os.path.expanduser("~"))
        correos = open(user+'/.mailbox_redes', 'a')
        correos.write(self.correo) 
        print("correo guardado")
    def verifica_msg(self,msg) -> None:
        """ Guarda el mensaje y selecciona la respuesta adecuada e este
            msg - mensaje
        """
        self.correo += msg+"\n"
        if len(msg) == 0:
            msg = "555 ERROR"
        elif msg == "QUIT: ":
            msg = "221 Bye"
        elif msg.startswith("DATA"):
            msg = "354 Start mail input; end with <CRLF>.<CRLF>"
        else:
            msg = "250 Ok"
            
        return msg

    def escuchar(self) -> None:
        # Establecemos el modo escucha
        self.mi_socket.listen(5)
        while True:
            # Indicamos que aceptamos conexiones del mundo exterior,
            # y esperamos conexiones entrantes
            (self.newcliente, addr) = self.mi_socket.accept()

            # Una vez que se lleva a cabo una conexión
            while True:
                # preparamos el buffer de datos para recibir información
                peticion = self.newcliente.recv(1024)

                # Enviamos un mensaje al cliente
                mensaje = peticion.decode()
                print("C: "+mensaje)
                mensaje = self.verifica_msg(mensaje)
                print("S: "+mensaje)
                self.newcliente.send(mensaje.encode('utf-8'))
                if mensaje == "221 Bye" or mensaje == "555 ERROR":
                    msg = "Correo enviado con éxito" if mensaje == "221 Bye" else "Error al enviar el correo"
                    print(msg)
                    self.guardar_correo()
                    self.newcliente.close()
                    self.mi_socket.close()
                    return 
        
if __name__ == "__main__":
    servidor = Servidor()
    servidor.escuchar()
    servidor.mi_socket.close()