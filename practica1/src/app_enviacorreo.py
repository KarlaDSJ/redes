import socket
import sys


class AppCorreo:
    """
    AppCorreo

    Nos permite enviar un correo
    param[0] = destinatario
    param[1] = -s (indica que pasaremos el subject del mensaje)
    param[2] = Subject 
    """
    def __init__(self) -> None:
        self.param = sys.argv 
        self.param.pop(0) #Quitamos el nombre del archivo
        self.mi_socket = socket.socket() # Instanciar la clase socket
        self.respuesta = "250 OK"
        self.correo = {"ELHO": "ciencias.com.mx CRLF",
                        "MAIL FROM":"ejemplo@ciencias.unam.mx CRLF",
                        "RCPT TO": self.param[0] + " CRLF",
                        "DATA":"",
                        "": "",
                        "QUIT": ""}

    def conectar(self) -> None:
        """Crea una conexión con el servidor"""
        # Nos conectamos al socket
        host = socket.gethostbyname(socket.gethostname())
        self.mi_socket.connect((host, 10000))

    def enviar_mensaje(self, msg) -> None:
        """Manda un mensaje al servidor y muestra la respuesta"""
        # Enviamos los mensajes, transformando el string en un objeto de tipo bytes
        print("C: "+msg)
        self.mi_socket.send(msg.encode('utf-8'))
        self.respuesta = self.mi_socket.recv(1024).decode()
        print("S: "+self.respuesta)

    def get_cuerpo(self) -> None:
        """Crea una cadena con el formato correcto para el cuerpo
           del mensaje, lee varias líneas, para indicar que termina
           el cuerpo del mensaje es necesario que el usuario ingrese 
           la cadena CRLF
        """
        aux = input("Cuerpo del mensaje: ")
        while not aux.endswith("CRLF"):
            self.correo[""] += aux+"\n"
            aux +="\n"+ input()
        self.correo[""] = "SUBJECT: " + self.param[2]+"\n"+ self.correo["DATA"]+aux

    def enviar_correo(self) -> None:
        """Envia todos los datos del correo al servidor"""
        if(len(self.param) == 3):
            self.get_cuerpo()
            i = 0
            while self.respuesta != "221 Bye" and self.respuesta != "555 ERROR":
                key = list(self.correo.keys())[i]
                self.enviar_mensaje(self.correo[key] if key == "" else key + ": " + self.correo[key])
                i+=1
            self.mi_socket.close()
            return
        else:
            print('Faltan datos')
            print('Se esperaba: app_enviacorreo usuario@example.com -s "Subject"')

if __name__ == "__main__":
    app = AppCorreo()
    app.conectar()
    app.enviar_correo()