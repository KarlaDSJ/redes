# Práctica 1
 
Implementación de una aplicación cliente/servidor de correo básico en Python
 
## Ejecución
Ejecutar en terminal al mismo tiempo en ventanas diferentes
- Para el servidor
```
cd redes
python3 practica1/src/Servidor.py
```
- Para la app de correo
```
cd redes
python3 practica1/src/app_enviacorreo.py usuario@example.com -s "Subject"
```
 
## Protocolo SMTP
Utilizará el protocolo SMTP (Simple Mail Transfer Protocol)
 
*Comandos utilizados:*
- EHLO (HELLO) - Utilizado para identificar el SMTP del cliente y del servidor, contiene el dominio del cliente
- MAIL FROM: < remitente > - Indica el correo del usuario que envía el correo
- RCPT (Recipient) TO < destinatario > -  Indica el correo del usuario que recibirá el correo
- DATA: - Cuerpo del mensaje
- < CRLF > - Indica el término de una línea (los comandos anteriores a excepción del primero terminan con estos símbolos)
- QUIT - Indica al servidor que ha terminado
 
*Respuestas:*
- "250 Ok" - Servidor responde que todo salió bien
- "354 OK" - Al recibir el cuerpo del correo
- "221 OK" - Servidor cierra el canal de transmisión
- "555" - No reconoce los comandos o parámetros utilizados
- "455" - El servidor no pudo acomodar los parámetros
 
###  Comentarios
- Tomamos como base el código de Omar Martinez [python-sockets]( https://gitlab.com/omazuru/python-sockets )

- Al ejecutar app_enviacorreo el cuerpo del mensaje debe terminar con CRLF 