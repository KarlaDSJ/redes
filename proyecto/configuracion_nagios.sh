#!/bin/bash

# Este script debe ejecutarse como root
if [ "$EUID" -ne 0 ]
  then echo "Por favor, corre este script como root"
  exit
fi

# CONFIGURACION DE RDP
echo "Configurando monitoreo de RDP"

git clone https://github.com/dnozay/check_x224.git
mv check_x224/check_x224.py /usr/local/nagios/libexec/
chown www-data:nagios /usr/local/nagios/libexec/check_x224.py
echo "
# Comando para monitorear conexiones por el protocolo RDP

define command {
    command_name    check_x224
    command_line    \$USER1\$/check_x224 -H \$HOSTADDRESS\$
}
" >> /usr/local/nagios/etc/objects/commands.cfg
echo "
# Servicio que monitorea conexiones RDP

define service {
    use                     local-service
    host_name               localhost
    service_description     RDP
    check_command           check_x224
    notifications_enabled   0
}
" >> /usr/local/nagios/etc/objects/localhost.cfg

# REINICIAMOS NAGIOS
systemctl restart nagios

echo "Servidor Nagios configurado c:"